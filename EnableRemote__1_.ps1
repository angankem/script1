# This URl is taken directly from Ninite website the commented URL is the path I set on Gitlab for testing purposde
#https://gitlab.com/angankem/remoteexe/-/archive/main/remoteexe-main.zip

$url ="https://ninite-tools.s3.amazonaws.com/EnableRemote.exe"

# Establish the path where the msi file will be store

$filePath = "C:\Temp\EnableRemote.exe"


# Download the EnableRemote.exe file

Invoke-WebRequest -Uri $url -OutFile $filePath


# Install EnableRemote.exe silently

Start-Process -FilePath $filePath -ArgumentList "/silent"


# Wait for installation to finish

Start-Sleep -Seconds 5


# Remove EnableRemote.exe from the file system. If this line is commented out we should be able to find the exe file in temp directory

#Remove-Item -Path $filePath 

